export const configuration = {
    CLIENT_API_URL: process.CLIENT_API_URL || 'http://localhost:8080',
    RECEPTION_API_URL: process.CLIENT_API_URL || 'http://localhost:8081',
    DELIVERY_API_URL: process.CLIENT_API_URL || 'http://localhost:8082',
    EBS_API_URL: process.EBS_API_URL || 'http://localhost:8083'
  }
  